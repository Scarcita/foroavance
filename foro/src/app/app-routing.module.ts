import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForoComponent } from './components/foro/foro.component';
import { InicioComponent } from './components/inicio/inicio.component';


const routes: Routes = [
  {path: 'inicio', component: InicioComponent},
  {path: 'foro', component: ForoComponent},
  {path: "**", pathMatch: 'full', redirectTo: 'inicio'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
