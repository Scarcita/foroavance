import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from '../servicios/validadores.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  forma!: FormGroup

  constructor(private fb: FormBuilder,
    private validadores: ValidadoresService) { 
    this.crearFormulario();
    this.cargarDataFormulario2();
  }

  ngOnInit(): void {
  }

  crearFormulario(): void{
    this.forma = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      apellido: ['', [Validators.required, Validators.minLength(4),Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      correo:['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
      usuario: ['', Validators.required, this.validadores.existeUsuario],
    }), {
      Validators: this.validadores.passwordsIguales ('pass1', 'pass2')
    } as AbstractControlOptions


      
  }

  guardar(): void{
    if(!this.forma.valid){
      return;
    }
    console.log(this.forma.value);
    this.LimpiarFormulario();
    
  }

  get nombreNoValido() {
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }

  get apellidoNoValido(){
    return this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.touched;
  }

  get correoNoValido(){
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
  }

  get pass1Novalido(){
    return this.forma.get('pass1')?.invalid && this.forma.get('pass1')?.touched;
  }

  get pass2Novalido(){
    return this.forma.get('pass2')?.hasError('noEsIgual');
    // const pass1 = this.forma.get('pass1')?.value;
    // const pass2 = this.forma.get('pass2')?.value;

    // return (pass1 === pass2) ? false : true;
  }

  get usuarioNoValido(){
    return (this.forma.get('usuario')?.invalid && !this.forma.get('usuario')?.errors?.['existe']) && this.forma.get('usuario')?.touched;
  }

  get usuarioIgualJerjes(){
    return this.forma.get('usuario')?.errors?.['existe'];
  }




  cargarDataFormulario():void {
    this.forma.setValue({
      nombre: 'scarleth',
      apellido: 'guzman',
      correo: 'guzman.sm7@gmail.com',
    });
  }
  cargarDataFormulario2():void {
    this.forma.patchValue({
      apellido: 'guzman',
      
    });
  }

  LimpiarFormulario():void{
    this.forma.reset({
      nombre: 'Scarleth'
    });
  }



}
