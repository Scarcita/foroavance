import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // usuario : string = '';
  // contrasena: string = '';
  // rol : string = '';
  // roles : string[];


  form!: FormGroup;


  constructor(private fb: FormBuilder) {
    this.formulario();
    // this.roles = [
    //   'admin',
    //   'user'
    // ]
   }

  ngOnInit(): void {
  }
  
  // login() {
  //   console.log(this.usuario+" "+this.contrasena+" "+this.rol); 
  // }

  formulario(): void {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


}
